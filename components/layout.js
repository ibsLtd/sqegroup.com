import React, {useEffect} from "react";
import NextHead from 'next/head'
import Header from '../components/header'
import Footer from '../components/footer'

const Layout = ({children}) => {


    useEffect(() => {

        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/service-worker.js')
                .then(registration => {
                    console.log('Service Worker registration successful')
                })
                .catch(err => {
                    console.warn(err.message)
                })
        }
    }, [])


    return (
        <React.Fragment>

            <NextHead>
                <meta charSet="UTF-8"/>
                <title>{'SQE Group'}</title>
                <meta name="description" content={'SQE Group is a leading world class provider of Safety, Quality Environmental Solutions for the shipping industry, on a mission to embrace excellence, for more than 20 years, serving a clientele in excess of 1,400 clients based in more than 75 countries!'}/>
                <meta name="keywords" content={'sqegroup,SQE,sqe,group,safety4sea'}/>
                <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
                <meta name="theme-color" content="#fff"/>

                <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com" crossOrigin={'true'}/>
                <link rel="dns-prefetch" href="//cdnjs.cloudflare.com" crossOrigin={'true'}/>
                <link rel="dns-prefetch" href="//ajax.googleapis.com" crossOrigin={'true'}/>
                <link rel="dns-prefetch" href="//s.w.org" crossOrigin={'true'}/>

                <link
                    href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
                    rel="stylesheet"></link>
                <link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
                      rel="stylesheet"></link>

                <link rel="manifest" href={"/manifest.json"}></link>
                <link rel="shortcut icon" href="/favicon.ico"></link>
                <link type="text/css" rel="stylesheet" href={"/style.css"}></link>

            </NextHead>

            <Header/>

            {children}
            <Footer/>
        </React.Fragment>
    )
}
export default Layout
