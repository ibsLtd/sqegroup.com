import React from 'react'


export const Welcome = ({items,onClick}) => {

    return (
        <div className={'row'}>
            {items.map((item, i) => {
                return (
                    <div key={i} className={'col-sm-6 col-md-3 custom-col-lg'} onClick={(e)=>onClick(e,item)} >
                        <img src={item.icon} alt={item.title} className={'icons'} />
                        <h3>{item.title}</h3>
                    </div>
                )
            })}
        </div>
    )

}