import React from 'react'


export const Description = ({item, onClick}) => {


    return (

        <div className="modal-background">
            <div className={'modal'}>
                <div className="d-flex flex-wrap flex-nowrap">
                    <img src={item === null ? null : item.description.img} alt={item === null ? null : item.title}/>
                    <div style={{position: 'relative'}}>
                        <h1>{item === null ? null : item.title}</h1>
                        <p>{item === null ? null : item.description.content}</p>
                        <div className={'d-flex buttons'}>

                            <a className={'read-more  draw'} href={item === null ? null : item.description.link}
                               target={'_blank'}>{'Explore More'}</a>

                            <div className={'close draw'} onClick={(e) => onClick(e, null)}>
                                <p>{'Close'}</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    )
}