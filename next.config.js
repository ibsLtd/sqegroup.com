const withCSS = require('@zeit/next-css')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')


module.exports = withCSS({
    exportPathMap: (defaultPathMap) => {
        return {
            '/': {page: '/'},
        }
    },
    exportTrailingSlash: true,
    optimization: {
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            automaticNameMaxLength: 30,
            name: true,
            cacheGroups: {
                vendors: {
                    chunks: 'all',
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                framework: {
                    name: 'framework',
                    chunks: 'all',
                    test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
                    priority: -20
                },

                lib: {
                    test(module) {
                        return module.size() > 20000
                    },
                    name(module) {
                        return /node_modules\/(.*)/.exec(module.identifier())[1]
                            .replace(/\/|\\/g, "_")
                    },
                    priority: -30,
                    minChunks: 1,
                    reuseExistingChunk: true
                },
                commons: {
                    name: 'commons',
                    chunks: 'all',
                    minChunks: 2,
                    priority: -40,
                    enforce: true
                },
                shared: {
                    name: false,
                    priority: -50,
                    minChunks: 2,
                    reuseExistingChunk: true
                },
                default: {
                    minChunks: 2,
                    priority: -60,
                    reuseExistingChunk: true
                }
            }
        }

    },

    webpack: config => {
        // Fixes npm packages that depend on `fs` module
        config.node = {
            fs: 'empty'
        },
            config.plugins.push(
                new SWPrecacheWebpackPlugin({
                    verbose: true,
                    staticFileGlobsIgnorePatterns: [/\.next\//],
                    cacheId: 'my-sw',
                    filename: 'service-worker.js',
                    runtimeCaching: [
                        {
                            handler: 'networkFirst',
                            urlPattern: /^https?.*/
                        }
                    ],
                    staticFileGlobs: [],
                    minify: true
                }),


            )



        return config

    }
})
