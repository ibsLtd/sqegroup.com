import React, {useState, useEffect} from 'react'
import json from '../public/index.json'
import Layout from "../components/layout";
import {Welcome} from "../components/welcome";
import {Description} from "../components/description";

export default function Home() {
    const [state, setState] = useState({isShowing: false, item: {},isDisplay:false})

    useEffect(()=>{
        window.addEventListener('load', handleModal)

        return () => {
            window.removeEventListener('load', handleModal)
        };

    })
    const handleModal = ()=>{
        setState({...state,isDisplay: true})
    }
    const onClick = (e, item) => {
        e.preventDefault()
        setState({...state, isShowing: !state.isShowing, item: item})
    }
    return (
        <Layout>

            <section className={'container'}>
                <div className={'icons-container'}>
                    <Welcome items={json} onClick={onClick}/>
                </div>
            </section>
            {state.isDisplay &&
                <section className={`modalDialog ${state.isShowing ? 'active' : ''} `}>
                    <Description item={state.isShowing ? state.item : null} onClick={onClick}/>
                </section>
            }



        </Layout>
    )
}
